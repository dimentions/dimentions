/*******************************************************************
// avion-ravitailleur
//suis avion1 et redonne essence
*******************************************************************/
function elt_ravitailleur_load(elt)// a transformer en elt.
{
//--------[collision]--------//
elt.cpt.fct['collision']=function(elt,data)
	{
	if(!data){data={};}
	if(!elt.cpt.iteration['collision']){elt.cpt.iteration['collision']=0;}elt.cpt.iteration['collision']++;

	if(data.obstacle)
		{
		switch(data.obstacle.genre)
			{
			case'panneau':
//				elt.cpt.CeC='ecrasement';
//				elt.cpt.actionFlag['ecrasement']=1;
//				elt.cpt.fct['ecrasement'](elt);
				break;
			case 'avion':
				data.obstacle.cpt.CeC=0;	//elt2:avion
				data.obstacle.cpt.actionFlag['ecrasement']=0;
				break;
			default:
				break;
			}
		}

	}

elt.cpt.fct[0]=function(elt)
	{
	if(!elt.cpt.iteration[0])
		{
		elt.cpt.iteration[0]=0;
		gestLib.write({lib:'dimention',txt:'avion: en vol'});
		}
	elt.cpt.iteration[0]++;
	if(elt.statut<0){return elt.statut;}
	//[age]
	//[normal]
	elt_cpt_normal.vaVersElt(elt,elt.idJS_parent.elt['avion1']);
	elt_cpt_normal.walk(elt);
	//[limite]
	elt_cpt_limite.traverseY(elt);
	elt_cpt_limite.traverseX(elt);
	elt_cpt_limite.neverOut(elt);
	//[affichage]
	elt_cpt_affichage.affichage0(elt);
	//[condition de changement de comportement]
	}
elt.cpt.fct['ecrasement']=function(elt)
	{
	if(!elt.cpt.iteration['ecrasement'])
		{
		elt.cpt.iteration['ecrasement']=0;
		gestLib.write({lib:'dimention',txt:'avion: debut de l\'ecrasement!'});
		}
	elt.cpt.iteration['ecrasement']++;

	if(elt.statut<0){return elt.statut;}
	//[age]
	if(elt.age.cycle>360){elt.age.cycle=0;elt.age.cycleNb++;}
	//[normal]
	elt_cpt_normal.moveRelXY(elt,-5,1);
	elt_cpt_normal.moveCosXY(elt,0,10);
	//elt_cpt_normal.walk(elt);
	//[limite]
	elt_cpt_limite.traverseX(elt);
	elt_cpt_limite.neverOut(elt);
	//[affichage]
	elt_cpt_affichage.affichage0(elt);
	//[condition de changement de comportement]
	if(elt.coordY+elt.idCSS.height>=elt.idCSSjq_parent.height())
		{elt.cpt.CeC='die';
		}
	}
elt.cpt.fct['die']=function(elt)
	{
	if(!elt.cpt.iteration['die'])
		{
		elt.cpt.iteration['die']=0;
		gestLib.write({lib:'dimention',txt:'avion: mort!'});
		}
	elt.cpt.iteration['die']++;

	if(elt.statut<0)
		{
		gestLib.write({lib:'dimention',txt:'avion:deja mort!'});
		return elt.statut;
		}
	//[age]
	if(elt.age.cycle>360){elt.age.cycle=0;elt.age.cycleNb++;}
	//[normal]
	//[limite]
	elt_cpt_limite.traverseX(elt);

	//[affichage]
	//elt.idCSSjq.css({display:'none'});
	elt_cpt_affichage.affichage0(elt);
	elt.idCSS.src='/intersites/lib/perso/js/dimentions/img/14671494-croix-noire-sur-fond-grunge.png';
	elt.idCSS.width=elt.idCSSjq.width();
	elt.idCSS.height=elt.idCSSjq.height();
	elt.statut=-1;//mort!
	gestLib.write({lib:'dimention',txt:'avion:mort!'});
	//[condition de changement de comportement]
	}

	elt.cpt.fct[2]=function(elt)
		{
		elt.idJS_parent.elt['prototype'].cpt.fct[2](elt,elt.idJS_parent.elt['avion1']);
		if(elt.vecteurX<0){elt.idCSS.src='/intersites/images/pictogrammes/collection1/120px-EA-18G.svg.png';}
		if(elt.vecteurX>0){elt.idCSS.src='/intersites/images/pictogrammes/collection1/120px-EA-18G.svg.png';}
		}
}//function elt_avion_init()
