/*!******************************************************************
fichier: ruches-0.2.2.js
version : 0.2.2
auteur : Pascal TOLEDO
date :1 decembre 2012
source: http://www.legral.fr/intersites/lib/perso/js/gestionScenario/
idee original:http://www.javascriptfr.com//code.aspx?ID=54636
description: vie d'une ruche
compatibilite: le doctype doit etre <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
dependance:aucune
*******************************************************************/
ruches_version='0.2.2';
/*******************************************************************
// - init{nomVar:'nom_de_la_var_instance'}
*******************************************************************/
gestDimention=function(init)
	{
	if(!init ||!init.idHTML){return null;}

	this.idHTML=(init.idHTML);
	this.idCSS=document.getElementById(this.idHTML);
//	this.idCSSjq='#'+this.idHTML;					// id de l'element CSS inserrer

	this.nom=(init.nom)?init.nom:this.idHTML;

	this.vieInterval=(init.vieInterval)?init.vieInterval:100;	//temps en ms en chaque appelle de vie()
	this.timerVieId=null;
	this.elt=Array();	//tableau de pointeur js sur chaque elt
	this.cycle=new Dimentioncycle(init.cycle);

	if(init.CSS)
		{
		if(init.CSS.height)this.idCSS.style.height=init.CSS.height+'px';
		if(init.CSS.width) this.idCSS.style.width= init.CSS.width+'px';
		}


	this.afficheInfoNo=0;
	this.afficheInfo=function()
		{
		gestLib.clear('consoleVie');
		var t='';
		t+='<b>Cycle</b><ul>';
			t+='<li>consigne:'+this.cycle.consigne+'</li>';
			t+='<li>iteration:'+this.cycle.iteration+'</li>';
			t+='<li>deb:'+this.cycle.timeStamp_deb+'</li>';
			t+='<li>reel:'+this.cycle.reel+'</li>';
			t+='<li>delta:'+this.cycle.delta+'</li>';
			t+='<li>avg_pointNb:'+this.cycle.avg_pointsNb+'/ '+this.cycle.avg_pointsMax+'</li>';
			t+='<li>peakMin/avg/peakMax:'+this.cycle.peakMin+'&lt;'+this.cycle.avg()+'&lt;'+this.cycle.peakMax+'</li>';
		t+='</ul>';
		t+='</ul>';
		t+='CSS:<ul>';
			t+='<li>idCSSjq:'+this.idCSSjq+'</li>';
			t+='<li>title:'+this.title+'|'+this.idCSS.title+'</li>';
			t+='<li>display:'+this.idCSS.style.display+'</li>';
//			t+='<li>larg:'+this.idCSS.clientWidth+'</li>';
//			t+='<li>haut:'+this.idCSS.clientHeight+'</li>';
			t+='<li>larg:'+this.idCSS.style.width+'</li>';
			t+='<li>haut:'+this.idCSS.style.height+'</li>';
		t+='</ul>';

		t+='dimentionnelle:<ul>';
			t+='<li>vecteurX:'+this.vecteurX+'</li>';
			t+='<li>vecteurY:'+this.vecteurY+'</li>';
//			t+='<li>:'+this.+'</li>';
		t+='</ul>';

		t+="afficheInfoNo:"+this.afficheInfoNo+'<br>';
		gestLib.write({lib:'consoleVie',txt:t});

		// ====[call vie des abeilles]====//
		if(this.elt[this.afficheInfoNo]){this.elt[this.afficheInfoNo].afficheInfo();}
		
		}
	// ---------------- VIE! -----------------
	this.vie=function()
		{if(this.cycle.isPause===1){return;}
		this.cycle.start();
		if(this.cycle.etat===2){return;}
		for(var i in this.elt)
			{
			if((this.elt[i])&&(this.elt[i].statut>0))
				{
				//test de collision avec tous les elt de la dimention
				for(var i2 in this.elt)
					{
					if	(this.elt[i]!=this.elt[i2])
						{
						AX0=this.elt[i].coordX;	AX1=this.elt[i].coordX+this.elt[i].idCSS.width;
						AY0=this.elt[i].coordY;	AY1=this.elt[i].coordY+this.elt[i].idCSS.height;
						BX0=this.elt[i2].coordX;	BX1=this.elt[i2].coordX+this.elt[i2].idCSS.width;
						BY0=this.elt[i2].coordY;	BY1=this.elt[i2].coordY+this.elt[i2].idCSS.height;
						if(		((AX0<=BX1)&&(AX0>=BX0))&&((AY0<=BY1)&&(AY0>=BY0))//coin gauche	haut	(AX0-AY1)
							||	((AX1<=BX1)&&(AX1>=BX0))&&((AY0<=BY1)&&(AY0>=BY0))//coin droit	haut	(AX1-AY0)
							||	((AX0<=BX1)&&(AX0>=BX0))&&((AY1<=BY1)&&(AY1>=BY0))//coin gauche	bas	(AX0-AY1)
							||	((AX1<=BX1)&&(AX1>=BX0))&&((AY1<=BY1)&&(AY1>=BY0))//coin droit	bas	(AX1-AY1)
						  )
							{
							//this.elt[i].cpt.CeC='collision';	//annule la colisison doit etre active instantanement!
							this.elt[i].cpt.appelle ('collision',{obstacle:this.elt[i2]})
							this.elt[i2].cpt.appelle('collision',{obstacle:this.elt[i]})
							
							//gestLib.write({lib:'dimention',txt:'collision entre '+i+' et '+i2});
							}
						}
//					else{}
					}//fin test collision

				this.elt[i].vie();
				}
			else
				{
				a='mort';
				}
			this.cycle.next();
			this.afficheInfo();
			}
		}
	this.vie();
	return this;
	}	// gestDimention=function(init)

gestDimention.prototype.eltAdd=function(init)
		{
		if(!init){init={};}
//		initT=init;

		if(!init.idHTML){init.idHTML=Number.uuid();}
		init.idHTML_parent=this.idHTML;


		this.elt[init.idHTML]=new elt_prototype0(init);
		}

