Dimentioncycle=function(init)
	{
	if(!init){init={};}
	//public
	this.consigne=(init.consigne)?init.consigne:1000;	// consigne entre 2 appelles 
	this.callback=(init.callback)?init.callback:'';	// nom de la fonction(en txt) a appelle qd la consigne est atteinte
	//prive
	this.dt=null;
	this.timerId=null;
	this.etat=0;			// 0: stop;1:start;
	this.reel=0;			// consigne reel d'un cycle
	this.timeStamp_deb=0;		// 
	this.delta=0;			// diff de temps entre la consigne et la duree du cycle reel
	this.iteration=0;
	this.peakMin=undefined;	// valeur max atteinte laisser undefined
	this.peakMax=0;			// valeur min atteinte
	this.points=Array();	// valeur courante empile
	this.avg_pointsNb=0;
	this.avg_pointsMax=(init.avg_pointsMax)?init.avg_pointsMax:100;	//nbre de point max pour calc la moyenne
	this.start=function()	// 
		{
		dt=new Date();
		this.timeStamp_deb=dt.getTime();
		if(this.timerId!==null){clearTimeout(this.timerId);}
		this.timerId=setTimeout(this.callback,this.delta);
		}
		
	this.stop=function()	// 
		{
		this.iteration++;
		if(this.timerId!==null){clearTimeout(this.timerId);this.timerId=null;}
		dt=new Date();
		this.reel=dt.getTime()-this.timeStamp_deb;
		if(this.reel>this.peakMax){this.peakMax=this.reel;}
		if(this.peakMin===undefined){this.peakMin=this.reel;}
		if(this.reel<this.peakMin){this.peakMin=this.reel;}
		this.delta=this.consigne-this.reel;
		this.points[this.avg_pointsNb++]=this.reel;
		if(this.avg_pointsNb>=this.avg_pointsMax){this.avg_pointsNb=0;this.points=Array();}
		}
	this.isPause=0;
	this.setPause=function(on)
		{
		switch(on)
			{
			case 1:this.isPause=1;break;
			case 0:this.isPause=0;break;
			default:this.isPause=(this.isPause===1)
			?
			0
			:1;
			break;
			}		
		}

	this.next=function(){this.stop();this.timerId=setTimeout(this.callback,this.delta);}
	this.avg=function(){var m=0;var nb=0;for(var pt in this.points){if(typeof(pt!=='undefined')){m+=this.points[pt];nb++;}}return m/nb;}
	}
