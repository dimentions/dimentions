/*!******************************************************************
fichier: ruches-0.2.2.js
version : 0.2.2
auteur : Pascal TOLEDO
date :1 decembre 2012
source: http://www.legral.fr/intersites/lib/perso/js/gestionScenario/
idee original:http://www.javascriptfr.com//code.aspx?ID=54636
description: vie d'une ruche
compatibilite: le doctype doit etre <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
dependance:aucune
*******************************************************************/
ruches_version='0.2.2';
/*******************************************************************
// - attribut
*******************************************************************/
ruche_elt_attribut=
	{
	actuel:0
	,max:0
	,mutFD:0
	,mutFP:0
	,mutGn:0	//no de le generation de la mutation
	,moyenne:0
	}
function elt_age(init)
	{
	if(!init){init={};}
	this.cycleMax=(init.cycleMax)?init.cycleMax:0;
	this.secondeMax=(init.secondeMax)?init.secondeMax:0;
	this.cycleLast=0;
	this.cycleNb=0;
	this.cycle=0;	this.seconde=0;
	//this.NbCtoS=function(interval){return(1000/interval);}
/*
	this.add=function(interval)
		{this.cycle++;
		if(this.cycleLast+this.NbCtoS(interval)>=this.cycle)
			{
			this.seconde++;
			this.cycleLast=this.cycle;
			}
		}
*/	
	this.isLimite=function(){return((this.cycle>=this.cycleMax)||(this.seconde>=this.secondeMax));}
	return this;
	}

/*******************************************************************
// - comportement
*******************************************************************/
function elt_comportement(init)
	{
	if((!init)&&(!init.idJS_parent)){return;}
	this.idJS_parent=init.idJS_parent;
	this.CeC=(init.CeC)?init.CeC:0;	//Comportement en Cours
	this.actionFlag=Array();	//etat d'une action: 0:stop 1:encours 2:pause,etc
	this.iteration=Array();
	this.fct=Array();
	return this;
	}
elt_comportement.prototype.appelle=function(cptIndex,data)
	{
	if(typeof(this.fct[cptIndex])!=='function')
		{
		this.CeC=0;//retour a la fonction primaire
		cptIndex=0;
		}
	this.fct[cptIndex](this.idJS_parent,data);
	}

/*******************************************************************
//
// - arg obligatoire: aucun
//statut:-1:mort!;0:pas ne;1;en vie;;
*******************************************************************/
elt_prototype0=function(eltInit)
	{
	if(!eltInit){eltInit={};}
	// -- initialisation --
	this.idJS_parent=(eltInit.idJS_parent)?eltInit.idJS_parent:null;
	this.idHTML_parent=(eltInit.idHTML_parent)?eltInit.idHTML_parent:'body';
	this.idCSS_parent=document.getElementById(this.idHTML_parent);
	this.idCSSjq_parent=jQuery('#'+this.idHTML_parent);

	this.statut=(eltInit.statut)?eltInit.statut:0;//0:creer;1:dans le cylce de vie; -1:mort

	this.idHTML=(eltInit.idHTML)?eltInit.idHTML:Number.uuid();
	//this.idCSS=document.getElementById(this.idHTML);
	this.idCSS=null;
	this.idCSSjq=null;					// id de l'element CSS inserrer

	this.nom=(eltInit.nom)?eltInit.nom:this.idHTML;

	this.src=(eltInit.src)?eltInit.src:null;
	this.imgClasse=(eltInit.imgClasse)?eltInit.imgClasse:null;
	this.title=(eltInit.title)?this.title:this.nom;
	

	this.coordX=(eltInit.coordX)?eltInit.coordX:0;
	this.coordY=(eltInit.coordY)?eltInit.coordY:0;
	this.vitesse=(eltInit.vitesse)?eltInit.vitesse:1;//mutiplcateur vectoriel
	this.vecteurX=(eltInit.vecteurX)?eltInit.vecteurX:0;
	this.vecteurY=(eltInit.vecteurY)?eltInit.vecteurY:0;

	this.haut=(eltInit.haut)?eltInit.haut:0;	//hauteur afficher de l'image(conversion) sans unite (px)
	this.larg=(eltInit.larg)?eltInit.larg:0;	// 

	// param specifique a l'IA
	this.age=new elt_age();
	this.genre=(eltInit.genre)?eltInit.genre:'generique0';

	this.cpt=new elt_comportement({idJS_parent:this,CeC:eltInit.CeC});
	
	/*******************************
	****** METHODE A SURCHARGER ****
	*******************************/
	this.vie=function()
		{
		if(this.statut===1)
			{
			this.age.cycle++;
			this.cpt.appelle(this.cpt.CeC);
			}
		}

	//----[designation des comportements]----//
	this.cpt.fct[0]=function(elt)
		{
		//[age]
		//[normal]
		elt_cpt_normal.vecteurAleatX(elt,1,10);
		elt_cpt_normal.vecteurAleatY(elt,10,20);
		elt_cpt_normal.walk(elt);
		
		//[limite]
		elt_cpt_limite.rebondiX(elt);
		elt_cpt_limite.rebondiY(elt);
		if(elt.vecteurX<0){elt.idCSS.style.src='/intersites/lib/perso/js/dimentions/img/Abeille1.gif';}
		if(elt.vecteurX>0){elt.idCSS.style.src='/intersites/lib/perso/js/dimentions/img/Abeille2.gif';}
		elt_cpt_limite.neverOut(elt);
		elt_cpt_affichage.affichage0(elt);
		}
	this.cpt.fct[1]=function(elt)
		{
		//[age]
		//[normal]
//		elt_cpt_normal.vecteurAleatX(elt,1,10);
//		elt_cpt_normal.vecteurAleatY(elt,10,20);
//		elt_cpt_normal.walk(elt);
		
		//[limite]
		elt_cpt_limite.rebondiX(elt);
		elt_cpt_limite.rebondiY(elt);
		if(elt.vecteurX<0){elt.idCSS.style.src='/intersites/lib/perso/js/dimentions/img/Abeille1.gif';}
		if(elt.vecteurX>0){elt.idCSS.style.src='/intersites/lib/perso/js/dimentions/img/Abeille2.gif';}
		elt_cpt_limite.neverOut(elt);
		elt_cpt_affichage.affichage0(elt);
		}
	this.cpt.fct[2]=function(elt,elt2)
		{
		//[age]
		//[normal]
		elt_cpt_normal.vaVersElt(elt,elt2);
		elt_cpt_normal.walk(elt);
		
		//[limite]
		elt_cpt_limite.rebondiX(elt);
		elt_cpt_limite.rebondiY(elt);
		elt_cpt_limite.neverOut(elt);
		elt_cpt_affichage.affichage0(elt);
		if(elt.vecteurX<0){elt.idCSS.style.src='/intersites/lib/perso/js/dimentions/img/Abeille1.gif';}
		if(elt.vecteurX>0){elt.idCSS.style.src='/intersites/lib/perso/js/dimentions/img/Abeille2.gif';}
		}


	/*************************
	****** INITIALISATION ****
	**************************/
	this.creerCSS();
	return this;
	}
	
elt_prototype0.prototype=
	{
	creerCSS:function()
		{
		this.idCSSjq_parent.append('<img id="'+this.idHTML+'" src="'+this.src+'" alt="'+this.nom+'" title="'+this.title+'"/>');
		this.idCSS=document.getElementById(this.idHTML);
		if(this.idCSS===null)
			{return null;}
		//this.idCSSjq=jQuery('#'+this.idHTML);
		with(this.idCSS)
			{src=this.src;
			title=this.title;
			}
		with(this.idCSS.style)
			{
			display=(this.statut>0)?'block':'none';
			position='absolute';
			overflow='hidden';
			left=this.coordX+'px';top:this.coordY+'px';
			height=this.haut+'px';
			width=this.larg+'px';
			cursor='help';
			visibility='visible';
			}
		}
//	,show:function(){this.idCSSjq.css({visibility:'visible'});}
	
	,afficheInfo:function()
		{
		gestLib.clear('consoleVie');
		var t='';
		t+='<img src="'+this.src+'"height="'+this.haut+'px" width="'+this.larg+'px" />';
		t+='age:<ul>';
			t+='<li><b>seconde: s/max:</b>'+this.age.seconde+'</li>';
			t+='<li><b>max: </b>'+this.age.max+'</li>';
			t+='<li>cycle:'+this.cycle+'</li>';
			t+='<li>cycleMax:'+this.cycleMax+'</li>';
			t+='<li>cycleNb:'+this.cycleNb+'</li>';
			t+='<li>cycleLast:'+this.cycleLast+'</li>';
		t+='</ul>';

		t+='proccessus:<ul>';
			t+='<li>idHTML:'+this.idHTML+'</li>';
			t+='<li>statut:'+this.statut+'</li>';
		t+='</ul>';
		t+='CSS:<ul>';
			t+='<li>idCSSjq:'+this.idCSSjq+'</li>';
		//	t+='<li>title:'+this.title+'|'+this.idCSS.title+'</li>';
			t+='<li>display:'+this.idCSS.style.display+'</li>';
		t+='</ul>';

		t+='dimentionnelle:<ul>';
			t+='<li>coordX:'+Math.round(this.coordX)+'</li>';
			t+='<li>coordY:'+Math.round(this.coordY)+'</li>';
			t+='<li>vecteurX:'+this.vecteurX+'</li>';
			t+='<li>vecteurY:'+this.vecteurY+'</li>';
//			t+='<li>:'+this.+'</li>';
		t+='</ul>';

		// dynamique
		t+='<ul>';
		for(var e in this)
			{
			t+='<li>'+e+':';
			t+='</li>';
			}
		t+='</ul>';
		t+='<hr>';
		gestLib.write({lib:'consoleVie',txt:t});
		}
	}
